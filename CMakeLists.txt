cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)

set(Qt5_DIR "E:/QT/5.13.0/msvc2017/lib/cmake/Qt5")
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

project(EntryDatabase LANGUAGES CXX)

find_package(Qt5 COMPONENTS REQUIRED Core Gui Widgets Sql)

#Paths
set(INCROOT ${PROJECT_SOURCE_DIR}/header)
set(SRCROOT ${PROJECT_SOURCE_DIR}/source)
set(UIROOT ${PROJECT_SOURCE_DIR}/ui)

#All source files
file(GLOB INCS "${INCROOT}/*.h") 
file(GLOB SRCS "${SRCROOT}/*.cpp")
file(GLOB UIS "${UIROOT}/*.ui")

set(CMAKE_AUTOUIC_SEARCH_PATHS ${UIROOT})

qt5_wrap_ui(ui_wrap ${UIS})
qt5_wrap_cpp(moc_sources ${INCS})

include_directories(${INCS})
include_directories (${PROJECT_BINARY_DIR})

#Add files
add_executable(EntryDatabase ${SRCS} ${INCS}
							 ${ui_wrap} 
							 ${moc_sources})

#Creating groups
source_group("Source Files" FILES ${SRCS})
source_group("Include Files" FILES ${INCS})
source_group("User Interfaces" FILES ${UIS})

# Link target to given libraries
target_link_libraries(EntryDatabase
	PUBLIC
	Qt5::Core
	Qt5::Gui 
	Qt5::Widgets
	Qt5::Sql)

if(WIN32)
	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Qt5::Core> $<TARGET_FILE_DIR:${PROJECT_NAME}>
		COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Qt5::Gui> $<TARGET_FILE_DIR:${PROJECT_NAME}>
		COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Qt5::Widgets> $<TARGET_FILE_DIR:${PROJECT_NAME}> 
		COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Qt5::Sql> $<TARGET_FILE_DIR:${PROJECT_NAME}> )
endif(WIN32)