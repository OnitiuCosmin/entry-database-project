#pragma once

#include "ui_mainwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_langBox_currentTextChanged(const QString &arg1);
	void on_submitButton_released();
	void on_agreeCheckBox_clicked();
	void showTime();
	void showDate();

private:
    Ui::MainWindow *ui;
};

