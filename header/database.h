#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include <qmessagebox.h>
#include <qfiledialog.h>
#include <QtGui>
#include <QtSql/qtsqlglobal.h>
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlerror.h>
#include <QtSql/qsqlquery.h>
#include <qdebug.h>

using namespace std;

class Database
{
public:

	bool open();
	bool close();
	static Database* getInstance();

	void query(QString command);

	~Database()=delete;

private:
	
	Database();
	static Database* myDatabase;                
	QSqlDatabase mydb;
};


