﻿#include <iostream>
#include <fstream>
#include <string>

#include "header/mainwindow.h"
#include "header/database.h"
#include "ui_mainwindow.h"
#include "Validate.h"

#include <qmessagebox.h>
#include <qfiledialog.h>
#include <QtGui>
#include <QtSql/qtsqlglobal.h>
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlerror.h>
#include <QtSql/qsqlquery.h>
#include <qdebug.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),	
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
	
	showTime();
	showDate();
	ui->countryBox->setCurrentIndex(138);
	
	QTimer *timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()),this,SLOT(showTime()));
	connect(timer, SIGNAL(timeout()), this, SLOT(showDate()));
	timer->start();

}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_langBox_currentTextChanged(const QString &arg1)
{
   if(arg1=="EN")
   { 
       ui->companyLabel->setText("Company");
       ui->countryLabel->setText("Country");
       ui->dateLabel->setText("Date"); 
       ui->firstNameLabel->setText("First name");
       ui->lastNameLabel->setText("Last name");
       ui->visitingLabel->setText("Visiting");
       ui->entranceLabel->setText("Entrance");
       ui->langLabel->setText("Language");
       ui->regulationLabel->setText("As per the General Data Protection Regulation, the company will keep the provided data for the period of six months from the fiiling in of this form.");
       ui->agreeLabel->setText("I acknowledge and respect the building evacuation rules and the work safety norms");       
	   ui->exitLabel->setText("In case of emergency, go to the exit, following the green Exit signs.");
	   ui->fireLabel->setText("In case of fire, use the stairwell instead of the elevator for the evacuation.");
	   ui->assemblyLabel->setText("In case of emergency, the assembly point is in the parking lot, next to the swimming pool.");
	   ui->cameraLabel->setText("The common spaces are under permanent video surveillance. Images are stored for one week.");
   }
   else 
   {
       ui->companyLabel->setText("Companie");
       ui->countryLabel->setText("Tara");
       ui->dateLabel->setText("Data"); 
       ui->firstNameLabel->setText("Nume");
       ui->lastNameLabel->setText("Prenume");
       ui->visitingLabel->setText("Pe cine viziteaza");
       ui->entranceLabel->setText("Intrarea");
       ui->langLabel->setText("Limba");
       ui->regulationLabel->setText("In conformitate cu Regulamentul General privind Protectia Datelor, compania va pastra datele furnizate pe perioada de 6 luni de la completarea prezentului formular");
       ui->agreeLabel->setText("Am luat la cunostinta si respect regulile de evacuare a cladirii si normele de securitatea muncii");
	   ui->exitLabel->setText("In caz de urgenta, indreptati-va spre iesire, urmarind semnele verzi Exit. ");
	   ui->fireLabel->setText("In caz de incendiu, folositi scarile in locul liftului pentru evacuare.");
	   ui->assemblyLabel->setText("In caz de urgenta, punctul de adunare este în parcare, in dreptul bazinului olimpic. ");
	   ui->cameraLabel->setText("Spatiile comune din incinta sunt sub supraveghere video permanenta.\n Imaginile sunt pastrate o saptamana.");



   }
}

void MainWindow::on_agreeCheckBox_clicked()
{
	if (ui->agreeCheckBox->isChecked())
	{
		ui->submitButton->setEnabled(true);
	}
	else
	{
		ui->submitButton->setDisabled(true);
	}

	if (ui->companyEdit->text().isEmpty() || ui->firstNameEdit->text().isEmpty() || ui->lastNameEdit->text().isEmpty() || ui->visitingEdit->text().isEmpty())
	{
		ui->submitButton->setDisabled(true);
	}
}

void MainWindow::showTime()
{
	QTime time = QTime::currentTime();
	QString time_text = time.toString("hh : mm : ss");
	ui->entranceEdit->setTime(time);
}

void MainWindow::showDate()
{
	QDate date = QDate::currentDate();
	ui->dateEdit->setDate(date);
}

void MainWindow::on_submitButton_released()
{
	QString firstName,lastName,country,company,visiting,time,date;
	ui->entranceEdit->setTime(QTime::currentTime());

	firstName = ui->firstNameEdit->text();
	lastName = ui->lastNameEdit->text();
	country = ui->countryBox->currentText();
	company = ui->companyEdit->text();
	visiting = ui->visitingEdit->text();
	time = ui->entranceEdit->time().toString();
	date = ui->dateEdit->date().toString();

	Validate checker;
	int validated=0;

	if (checker.stringValidate(firstName) == 0 && checker.stringValidate(lastName) == 0 && checker.stringValidate(visiting) == 0)
	{
		validated = 1;
	}


	if (validated == 1)
	{
		Database::getInstance()->query("INSERT INTO Visitators([First Name], [Last Name] ,Country, Company, Visiting, [Entrance time], [Entrance date]) " "VALUES('" + firstName + "', '" + lastName + "', '" + country + "', '" + company + "', '" + visiting + "', '" + time + "', '" + date + "')");

		ui->firstNameEdit->setText("");
		ui->lastNameEdit->setText("");
		ui->companyEdit->setText("");
		ui->visitingEdit->setText("");

		QMessageBox *msgBox = new QMessageBox(this);

		msgBox->setText("Entry accepted!\n Welcome");
		msgBox->setMinimumWidth(800);
		msgBox->setMinimumHeight(400);
		//msgBox->setWindowFlags(Qt::FramelessWindowHint);

		msgBox->exec();
	}
	else
	{
		QMessageBox *msgBox = new QMessageBox(this);

		msgBox->setText("Entry denied!\n Invalid lines");
		msgBox->setMinimumWidth(800);
		msgBox->setMinimumHeight(400);
		msgBox->exec();
	}
	
}