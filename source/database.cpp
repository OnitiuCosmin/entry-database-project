
#include <iostream>
#include <fstream>
#include <string>

#include "header/database.h"

#include <qmessagebox.h>
#include <qfiledialog.h>
#include <QtGui>
#include <QtSql/qtsqlglobal.h>
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlerror.h>
#include <QtSql/qsqlquery.h>
#include <qdebug.h>
#include <QSqlRecord>


bool Database::open()
{
	mydb = QSqlDatabase::addDatabase("QSQLITE");
	mydb.setDatabaseName("../Visitators.db");

	if (!mydb.open())
	{
		qDebug() << ("Failed to open Database");
		return false;
	}
	else
	{
		qDebug() << ("Connected.....");
		return true;
	}

}

bool Database::close()
{
	mydb.close();

	if (mydb.open())
	{
		qDebug() << ("Failed to close Database");
		return false;
	}
	else
	{
		qDebug() << ("Disconnected.....");
		return true;
	}
}


Database* Database::myDatabase;

Database * Database::getInstance()
{
	if (myDatabase == nullptr)
	{
		std::cout << "First use -> initialization\n";
		myDatabase = new Database();
	}
	else
	{
		std::cout << "Instance already exists -> no initalization\n";
	}

	return myDatabase;
}


void Database::query(QString command)
{
	QSqlQuery q;

	q.exec(command);
	
}

Database::Database()
{

}