#include "header/mainwindow.h"
#include "header/database.h"

#include<iostream>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QTextEdit>
#include<qtimer.h>
#include<qdatetime.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	
	Database::getInstance()->open();

	MainWindow w;
    w.showMaximized();

    return a.exec();
}
